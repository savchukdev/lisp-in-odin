package main

import "core:fmt"
import "core:os"
import "core:io"
import "core:strings"
import "core:unicode"
import c "core:c/libc"

Environment :: struct {
  store: map[string]^Value,
  parent: ^Environment,
}

environment_with_parent :: proc(parent: ^Environment) -> ^Environment {
  env := new(Environment)
  env.parent = parent

  return env
}

environment_put :: proc(env: ^Environment, key: string, value: ^Value) {
  env.store[key] = value
}

environment_get :: proc(env: ^Environment, key: string) -> ^Value {
  val, ok := env.store[key]

  if !ok && env.parent != nil {
    return environment_get(env.parent, key)
  }

  if val == nil {
    return Vnil
  }

  return val
}

Function :: struct {
  args: ^Value,
  body: ^Value,
  env: ^Environment,
}

Pair :: struct {
  car: ^Value, // header
  cdr: ^Value, // tail
}

Rational :: struct {
  numerator: int,
  denominator: int,
}

Symbol :: distinct string

Primitive :: #type proc(^Value) -> ^Value

Value :: union {
  bool,
  int,
  Rational,
  string,
  Primitive,
  Symbol,
  Function,
  Pair,
}

Qnil, Qtrue, Qfalse: Value
Vnil, Vtrue, Vfalse: ^Value

init_const :: proc() {
  Vnil = &Qnil
  Vtrue = &Qtrue
  Vfalse = &Qfalse

  Vtrue^ = true
  Vfalse^ = false
}

// a bunch of factory functions to instantiate ^Value out of different types
make_string :: proc(s: string) -> ^Value {
  v := new(Value)
  v^ = s
  return v
}

make_symbol :: proc(s: string) -> ^Value {
  v := new(Value)
  v^ = Symbol(s)
  return v
}

make_integer :: proc(i: int) -> ^Value {
  v: = new(Value)
  v^ = i
  return v
}

make_proc :: proc(pr: Primitive) -> ^Value {
  v: = new(Value)
  v^ = pr
  return v
}

make_function :: proc(args, body: ^Value, env: ^Environment) -> ^Value {
  v: = new(Value)
  v^ = Function { args, body, env }
  return v
}

make_pair :: proc(a, b: ^Value) -> ^Value {
  v: = new(Value)
  v^ = Pair { car = a, cdr = b }
  return v
}

// creates nested pairs out of given list, e.g:
// [4 5 6] -> Pair { 4, Pair { 5, Pair { 6, Vnil } } }
pairs_list :: proc(values: []^Value) -> ^Value {
  b := Vnil
  for i := len(values) - 1; i >= 0; i -= 1 {
    b = make_pair(values[i], b)
  }

  return b
}

// gcd stands for greatest common divisor
gcd :: proc(a, b: int) -> int {
  a := a
  b := b

  for b > 0 {
    t := a % b
    a = b
    b = t
  }

  return a
}

// instead of using imprecise float values we're going to build rational numbers.
// so 3 / 5 is going to be literally "3 / 5", with 3 in numerator and 5 in denominator
divide :: proc(a, b: ^Value) -> ^Value {
  a := a.(int)
  b := b.(int)
  g := gcd(a, b)

  v := new(Value)

  r := Rational {a/g, b/g}
  v^ = r

  return v
}

// suprisingly, odin doesn't have a utility function to read a line from stdin
// this is my very first attempt to implement said function
read_line :: proc(handle: os.Handle) -> ^string {
  stream := os.stream_from_handle(handle)
  reader := io.to_reader(stream)

  buf := make([dynamic]byte, 0)

  for b, err := io.read_byte(reader); b != 10; {
    if err != nil {
      return nil
    }
    append(&buf, b)
    b, err = io.read_byte(reader)
  }

  str := new(string)
  str^ = string(buf[:])

  return str
}

ch: rune

is_symbol :: proc(ch: rune) -> bool {
  return unicode.is_alpha(ch) || strings.contains_rune("+-*/", ch) != -1
}

// parses integer
read_integer :: proc(reader: ^strings.Reader) -> ^Value {
  result := 0
  for unicode.is_digit(ch) {
    result = result * 10 + int(ch) - '0'

    err: io.Error
    ch, _, err = strings.reader_read_rune(reader)
    if err != io.Error.None {
      break
    }
  }

  return make_integer(result)
}

// parses symbol. symbol is basically a string, but we're not surrounding it with
// quote characters when printing out
read_symbol :: proc(reader: ^strings.Reader) -> ^Value {
  builder: strings.Builder
  strings.builder_init_none(&builder)

  for is_symbol(ch) {
    strings.write_rune(&builder, ch)

    err: io.Error
    ch, _, err = strings.reader_read_rune(reader)
    if err != io.Error.None {
      break
    }
  }

  return make_symbol(strings.to_string(builder))
}

// parse list of values. values a separated by space
read_list :: proc(reader: ^strings.Reader) -> ^Value {
  list := make([dynamic]^Value, 0, 4)
  defer delete(list)

  skip_spaces(reader)

  for true {
    if ch == ')' {
      break
    }

    append(&list, read_value(reader))

    if ch == ' ' {
      skip_spaces(reader)
    }
  }

  skip_spaces(reader)

  return pairs_list(list[:])
}

lisp_read :: proc(reader: ^strings.Reader) -> ^Value {
  skip_spaces(reader)
  return read_value(reader)
}

read_value :: proc(reader: ^strings.Reader) -> ^Value {
  if unicode.is_digit(ch) {
    return read_integer(reader)
  }
  if is_symbol(ch) {
    return read_symbol(reader)
  }
  if ch == '(' {
    return read_list(reader)
  }

  return nil
}

// eats space characters
// WARNING: will eat at least one character no matter what current character is,
// be cautious when calling it
// current character is stored in global `ch` variable
skip_spaces :: proc(reader: ^strings.Reader) {
  err: io.Error
  ch, _, err = strings.reader_read_rune(reader)
  if err != nil {
    return
  }

  for unicode.is_space(ch) {
     err: io.Error
     ch, _, err = strings.reader_read_rune(reader)
     if err != nil {
       break
     }
  }
}

// print value into a handle, usually os.stdout
lisp_write :: proc(value: ^Value, handle: os.Handle) {
  switch operand in value {
    case bool:
      fmt.fprintf(handle, "%t", operand)
    case int:
      fmt.fprintf(handle, "%d", operand)
    case Rational:
      fmt.fprintf(handle, "%d/%d", operand.numerator, operand.denominator)
    case string:
      fmt.fprintf(handle, `"%s"`, operand)
    case Symbol:
      fmt.fprintf(handle, `%s`, operand)
    case Function:
      fmt.fprintf(handle, "(lambda ")
      lisp_write(operand.args, handle)
      fmt.fprintf(handle, " ")
      lisp_write(operand.body, handle)
      fmt.fprintf(handle, ")")
    case Primitive:

    case Pair:
      fmt.fprintf(handle, "(")
      val := value

      for true {
        lisp_write(val.(Pair).car, handle)
        val = val.(Pair).cdr

        if val == Vnil {
          break
        }

        ok: bool
        if _, ok = val.(Pair); !ok {
          fmt.fprintf(handle, " . ")
          lisp_write(val, handle)
          break
        }

        fmt.fprintf(handle, " ")
      }

      fmt.fprintf(handle, ")")
    case:
      fmt.fprintf(handle, "()")
  }
}

// cons, car, cdr and add represent the whole standard library lol
cons :: proc(args: ^Value) -> ^Value {
  assert(type_of(args) == Pair)
  args := args.(Pair)
  a := args.car
  b := args.cdr.(Pair).car

  return make_pair(a, b)
}

car := proc(args: ^Value) -> ^Value {
  assert(type_of(args) == Pair)
  args := args.(Pair)
  return args.car;
}

cdr := proc(args: ^Value) -> ^Value {
  assert(type_of(args) == Pair)
  args := args.(Pair)
  return args.cdr;
}

add :: proc(args: ^Value) -> ^Value {
  args := args.(Pair)

  a := args.car.(int)
  b := args.cdr.(Pair).car.(int)

  return make_integer(a + b)
}

// prepopulate environment with basic functions
init_env :: proc() -> ^Environment {
  env := new(Environment)
  environment_put(env, "cons", make_proc(cons))
  environment_put(env, "car", make_proc(car))
  environment_put(env, "cdr", make_proc(cdr))
  environment_put(env, "add", make_proc(add))
  environment_put(env, "+", make_proc(add))

  return env
}

eval :: proc(value: ^Value, env: ^Environment) -> ^Value {
  #partial switch operand in value {
    case bool:
      return value
    case int:
      return value
    case Rational:
      return value
    case string:
      return value
    case Symbol:
      return environment_get(env, string(operand))
  }

  value := value.(Pair)
  head := value.car
  tail := value.cdr

  if sym, ok := head.(Symbol); ok {
    if sym == "define" {
      sym := tail.(Pair).car.(Symbol)
      value := eval(tail.(Pair).cdr.(Pair).car, env)
      environment_put(env, string(sym), value)

      return Vnil
    } else if sym == "lambda" {
      return make_function(tail.(Pair).car, tail.(Pair).cdr, env)
    } else if sym == "begin" {
      return eval_seq(tail, env)
    }
  }

  head = eval(head, env)
  tail = eval_map(tail, env)
  return apply(head, tail)
}

eval_seq :: proc(value: ^Value, env: ^Environment) -> ^Value {
  b := Vnil
  value := value

  for value != Vnil {
    b = eval(value.(Pair).car, env)
    value = value.(Pair).cdr
  }

  return b
}

eval_map :: proc(value: ^Value, env: ^Environment) -> ^Value {
  list := make([dynamic]^Value, 0, 4)
  defer delete(list)
  value := value

  for value != Vnil {
    append(&list, eval(value.(Pair).car, env))
    value = value.(Pair).cdr
  }

  return pairs_list(list[:])
}

apply :: proc(a, b: ^Value) -> ^Value {
  if pr, ok := a.(Primitive); ok {
    return pr(b)
  }

  function := a.(Function)
  args := function.args
  env := environment_with_parent(function.env)

  v := b
  for k := args; k != Vnil; k = k.(Pair).cdr {
    environment_put(env, string(k.(Pair).car.(Symbol)), v.(Pair).car)
    v = v.(Pair).cdr
  }

  return eval_seq(a.(Function).body, env)
}

main :: proc () {
  init_const()
  env := init_env()
  defer free(env)

  for true {
    str := read_line(os.stdin)
    defer free(str)
    if str == nil {
      break
    }

    strings_reader: strings.Reader
    strings.to_reader(&strings_reader, str^)

    value := lisp_read(&strings_reader)
    result := eval(value, env)
    if result == Vnil {
      continue
    }
    lisp_write(result, os.stdout)
    fmt.println("")
  }
}
